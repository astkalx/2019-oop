public class Palindrome {

    public static Boolean isPalindrome(String s) {
        String rs = reverseString(s);
        return s.equals(rs);
    }

    private static String reverseString(String s){
        StringBuilder str = new StringBuilder(s);
        return str.reverse().toString();
    }
}

