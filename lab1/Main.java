import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scaner = new Scanner(System.in);
        for (int i = 0; i<args.length; i++) {
            String s = args[i];
            System.out.println(Palindrome.isPalindrome(s));
        }
        System.out.println("Enter final word : ");
        String finalString = scaner.next();
        scaner.close();
        System.out.println(Palindrome.isPalindrome(finalString));
    }
}
