import java.io.InputStream;
import java.net.MalformedURLException;

public class Main {
    public static void main(String[] args) throws MalformedURLException {
        if (args.length != 3) {
            System.out.print("ERROR: Not found <start_URL>, <max_depth> and <num_threads> argument\n");
            System.exit(1);
        }

        String startUrl;
        int maxDepth;
        int numThreads;

        startUrl = args[0];
        maxDepth = Integer.parseInt(args[1]);
        numThreads = Integer.parseInt(args[2]);

        Crawler crawler = new Crawler(startUrl, maxDepth, numThreads);
        crawler.run();
        crawler.printResult();
    }
}
