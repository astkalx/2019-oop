public class Point3d {
    private double X_cord;
    private double Y_cord;
    private double Z_cord;

    public Point3d() {
        //this.X_cord = 0.0;
        //this.Y_cord = 0.0;
        //this.Z_cord = 0.0;
        this(0,0,0);
    }

    public Point3d(double x, double y, double z){
        this.X_cord = x;
        this.Y_cord = y;
        this.Z_cord = z;
    }

    public void setX_cord(double x){
        this.X_cord = x;
    }
    public void setY_cord(double y){
        this.Y_cord = y;
    }
    public void setZ_cord(double z){
        this.Z_cord = z;
    }

    public double getX_cord(){
        return this.X_cord;
    }
    public double getY_cord(){
        return this.Y_cord;
    }
    public double getZ_cord(){
        return this.Z_cord;
    }


    public boolean equals(Point3d Object){
        return this.X_cord == Object.X_cord && this.Y_cord == Object.Y_cord && this.Z_cord == Object.Z_cord;
    }

    public double distanceTo(Point3d Object){
        double distance = Math.sqrt(
                Math.pow(Math.abs(this.X_cord - Object.X_cord), 2)
                        + Math.pow(Math.abs(this.Y_cord - Object.Y_cord), 2)
                        + Math.pow(Math.abs(this.Z_cord - Object.Z_cord), 2)
        );
        return distance;
    }

}
