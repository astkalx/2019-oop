import java.util.Scanner;

public class Main {
    public static void main(String args[]){
        Scanner scann = new Scanner(System.in);
        int num = scann.nextInt();
        for (int i = 1; i<=num; i++){
            if (Primes.isPrime(i)) {
                System.out.println(i);
            }
        }
    }
}
