public class Primes {
    public static boolean isPrime(int a) {
        int _temp = a-1;
        while (_temp >= 2) {
            if (a % _temp == 0) {
                return false;
            }
            _temp--;
        }
        return true;
    }
}